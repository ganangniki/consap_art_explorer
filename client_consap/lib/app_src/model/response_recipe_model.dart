import 'dart:convert';

ResponseRecipeList responseRecipeListFromJson(String str) =>
    ResponseRecipeList.fromJson(json.decode(str));

String responseRecipeListToJson(ResponseRecipeList data) =>
    json.encode(data.toJson());

class ResponseRecipeList {
  ResponseRecipeList({
    this.results,
    this.offset,
    this.number,
    this.totalResults,
  });

  List<Result> results;
  int offset;
  int number;
  int totalResults;

  factory ResponseRecipeList.fromJson(Map<String, dynamic> json) =>
      ResponseRecipeList(
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        offset: json["offset"],
        number: json["number"],
        totalResults: json["totalResults"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "offset": offset,
        "number": number,
        "totalResults": totalResults,
      };
}

class Result {
  Result({
    this.id,
    this.title,
    this.image,
    this.imageType,
  });

  int id;
  String title;
  String image;
  ImageType imageType;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        title: json["title"],
        image: json["image"],
        imageType: imageTypeValues.map[json["imageType"]],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "image": image,
        "imageType": imageTypeValues.reverse[imageType],
      };
}

enum ImageType { JPG }

final imageTypeValues = EnumValues({"jpg": ImageType.JPG});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
