import 'dart:convert';

List<ResponseFavorites> responseFavoritesFromJson(String str) =>
    List<ResponseFavorites>.from(
        json.decode(str).map((x) => ResponseFavorites.fromJson(x)));

String responseFavoritesToJson(List<ResponseFavorites> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ResponseFavorites {
  ResponseFavorites({
    this.id,
    this.title,
    this.image,
    this.imageType,
  });

  String id;
  String title;
  String image;
  String imageType;

  factory ResponseFavorites.fromJson(Map<String, dynamic> json) =>
      ResponseFavorites(
        id: json["id"],
        title: json["title"],
        image: json["image"],
        imageType: json["imageType"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "image": image,
        "imageType": imageType,
      };
}
