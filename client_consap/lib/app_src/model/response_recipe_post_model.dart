import 'dart:convert';

ResponseRecipePost responseRecipePostFromJson(String str) =>
    ResponseRecipePost.fromJson(json.decode(str));

String responseRecipePostToJson(ResponseRecipePost data) =>
    json.encode(data.toJson());

class ResponseRecipePost {
  ResponseRecipePost({
    this.id,
    this.title,
    this.image,
    this.imageType,
  });

  String id;
  String title;
  String image;
  String imageType;

  factory ResponseRecipePost.fromJson(Map<String, dynamic> json) =>
      ResponseRecipePost(
        id: json["id"],
        title: json["title"],
        image: json["image"],
        imageType: json["imageType"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "image": image,
        "imageType": imageType,
      };
}
