import 'dart:convert';
import 'package:consap_art_explorer/app_src/model/response_favorite_model.dart';
import 'package:consap_art_explorer/app_src/model/response_recipe_model.dart';
import 'package:consap_art_explorer/app_src/model/response_recipe_post_model.dart';
import 'package:http/http.dart' as http;

class ApiService {
  static const String API_KEY = '5471295ebb474d4bbfc0b671ecffbb1f';

  static Future fetchRecipes(bool isVegan) async {
    try {
      List<Result> recipes = [];
      final String dietUri = !isVegan ? 'primal' : 'vegan';
      final response = await http.get(Uri.parse(
          'https://api.spoonacular.com/recipes/complexSearch?apiKey=$API_KEY&number=50&diet=$dietUri'));

      final json = jsonDecode(response.body);
      ResponseRecipeList responseRecipes = ResponseRecipeList.fromJson(json);

      responseRecipes.results.forEach((item) {
        recipes.add(item);
      });

      recipes.shuffle();

      return recipes;
    } catch (err) {
      print(err);
    }
  }

  static Future postRecipe(
      int id, String title, String image, String imageType) async {
    var body = {
      'id': id.toString(),
      'title': title,
      'image': image,
      'imageType': imageType
    };

    try {
      final response = await http
          .post(Uri.parse('http://10.0.2.2:3000/favorites'), body: body);

      final json = jsonDecode(response.body);
      ResponseRecipePost responsePost = ResponseRecipePost.fromJson(json);

      return responsePost;
    } catch (err) {
      print(err);
    }
  }

  static Future fetchFavorites() async {
    try {
      List<ResponseFavorites> favorites = [];

      final response =
          await http.get(Uri.parse('http://10.0.2.2:3000/favorites'));

      final List data = jsonDecode(response.body);

      data.forEach((item) {
        ResponseFavorites responseFavorites = ResponseFavorites.fromJson(item);
        favorites.add(responseFavorites);
      });

      return favorites;
    } catch (err) {
      print(err);
    }
  }
}
