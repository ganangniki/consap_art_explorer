import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DetailApp extends StatelessWidget {
  const DetailApp({this.detailUri});
  final String detailUri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: EdgeInsets.symmetric(horizontal: 8),
          child: WebView(
            initialUrl: 'https://spoonacular.com/$detailUri',
            javascriptMode: JavascriptMode.unrestricted,
            gestureNavigationEnabled: false,
            navigationDelegate: this._interceptNavigation,
          )),
    );
  }

  NavigationDecision _interceptNavigation(NavigationRequest request) {
    return NavigationDecision.prevent;
  }
}
