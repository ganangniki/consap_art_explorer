import 'bloc/explore_bloc.dart';
import 'components/bottom_navigation_bar.dart';

import 'package:flutter_tindercard/flutter_tindercard.dart';
import 'package:provider/provider.dart';
import 'screens/explore_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ExploreApp extends StatelessWidget {
  final CardController controller = CardController();

  @override
  Widget build(BuildContext context) {
    return Provider<ExploreBloc>(
      create: (_) => ExploreBloc(),
      dispose: (_, ExploreBloc bloc) => bloc.dispose(),
      child: Scaffold(
          backgroundColor: Colors.grey[300],
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.grey[300],
            title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    child: SvgPicture.asset('assets/explore.svg',
                        height: 30, color: Colors.deepPurple[400]),
                  ),
                  GestureDetector(
                    onTap: () =>
                        Navigator.pushReplacementNamed(context, '/favorites'),
                    child: SvgPicture.asset('assets/favorite.svg',
                        height: 25, color: Colors.grey[500]),
                  ),
                  GestureDetector(
                    onTap: () =>
                        Navigator.pushReplacementNamed(context, '/account'),
                    child: SvgPicture.asset('assets/profile.svg',
                        height: 25, color: Colors.grey[500]),
                  )
                ]),
          ),
          body: ExploreScreen(controller: controller),
          bottomNavigationBar:
              CustomBottomNavigationBar(controller: controller)),
    );
  }
}
