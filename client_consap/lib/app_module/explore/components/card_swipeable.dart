// import 'dart:math';

import 'package:flutter/material.dart';

class CardSwipeable extends StatelessWidget {
  const CardSwipeable({this.imageSrc, this.recipeName, this.id});
  final String imageSrc, recipeName;
  final int id;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    String detailUri = recipeName.replaceAll(' ', '-') + '-$id';

    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  blurRadius: 5,
                  spreadRadius: 2)
            ]),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Stack(children: [
            Container(
              width: size.width,
              height: size.height,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(imageSrc), fit: BoxFit.cover)),
            ),
            Container(
              width: size.width,
              height: size.height,
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                Colors.black.withOpacity(0.25),
                Colors.black.withOpacity(0),
              ], end: Alignment.topCenter, begin: Alignment.bottomCenter)),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(
                              width: size.width * 0.65,
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(children: [
                                      Flexible(
                                        child: Text(recipeName,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 24,
                                                fontWeight: FontWeight.bold)),
                                      ),
                                    ]),
                                    SizedBox(height: 15),
                                    Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.white, width: 2),
                                            borderRadius:
                                                BorderRadius.circular(30),
                                            color:
                                                Colors.white.withOpacity(0.4)),
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 3,
                                              bottom: 3,
                                              left: 10,
                                              right: 10),
                                          child: Text('Easy to serve',
                                              style: TextStyle(
                                                  color: Colors.white)),
                                        )),
                                  ]),
                            ),
                            Expanded(
                                child: GestureDetector(
                              onTap: () => Navigator.pushNamed(
                                  context, '/detail/$detailUri'),
                              child: Container(
                                width: size.width * 0.2,
                                child: Center(
                                    child: Icon(Icons.info,
                                        color: Colors.white, size: 28)),
                              ),
                            ))
                          ]),
                    )
                  ]),
            )
          ]),
        ));
  }
}
