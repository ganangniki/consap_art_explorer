import 'package:consap_art_explorer/app_module/explore/bloc/explore_bloc.dart';
import 'package:consap_art_explorer/app_module/explore/components/bottom_nav_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tindercard/flutter_tindercard.dart';
import 'package:provider/provider.dart';

class CustomBottomNavigationBar extends StatelessWidget {
  const CustomBottomNavigationBar({this.controller});
  final CardController controller;

  @override
  Widget build(BuildContext context) {
    final ExploreBloc bloc = Provider.of<ExploreBloc>(context, listen: false);

    var size = MediaQuery.of(context).size;

    return Container(
        width: size.width,
        height: 120,
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
          child: StreamBuilder<bool>(
              stream: bloc.isVegan,
              builder: (context, snapshot) {
                if (!snapshot.hasData) return SizedBox();
                return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                          onTap: () {
                            snapshot.data
                                ? bloc.getRecipe(true)
                                : bloc.getRecipe(false);
                          },
                          child: BottomNavIcon(
                              buttonSize: 35,
                              iconSize: 20,
                              iconSrc: 'assets/reload.svg')),
                      GestureDetector(
                          onTap: () => controller.triggerLeft(),
                          child: BottomNavIcon(
                              buttonSize: 58,
                              iconSize: 25,
                              iconSrc: 'assets/close.svg')),
                      GestureDetector(
                          onTap: () => controller.triggerRight(),
                          child: BottomNavIcon(
                              buttonSize: 58,
                              iconSize: 28,
                              iconSrc: 'assets/like.svg')),
                      GestureDetector(
                          onTap: () {
                            if (!snapshot.data) {
                              bloc.isVeganListener(true);
                              bloc.getRecipe(true);
                            } else {
                              bloc.isVeganListener(false);
                              bloc.getRecipe(false);
                            }
                          },
                          child: Container(
                            width: 35,
                            height: 35,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.grey[300],
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey[600],
                                      offset: Offset(4.0, 4.0),
                                      blurRadius: 15.0,
                                      spreadRadius: 1.0),
                                  BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(-4.0, -4.0),
                                      blurRadius: 15.0,
                                      spreadRadius: 1.0),
                                ],
                                gradient: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                    colors: [
                                      Colors.grey[200],
                                      Colors.grey[300],
                                      Colors.grey[400],
                                      Colors.grey[500],
                                    ],
                                    stops: [
                                      0.1,
                                      0.3,
                                      0.8,
                                      1
                                    ])),
                            child: Center(
                                child: SvgPicture.asset('assets/vegan.svg',
                                    color: !snapshot.data
                                        ? Colors.grey[500]
                                        : Colors.green,
                                    width: 20)),
                          ))
                    ]);
              }),
        ));
  }
}
