import 'package:consap_art_explorer/app_module/explore/bloc/explore_bloc.dart';
import 'package:consap_art_explorer/app_src/model/response_recipe_model.dart';

import '../components/card_swipeable.dart';

import 'package:flutter/material.dart';
import 'package:flutter_tindercard/flutter_tindercard.dart';
import 'package:provider/provider.dart';

class ExploreScreen extends StatelessWidget {
  const ExploreScreen({this.controller});
  final CardController controller;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final ExploreBloc bloc = Provider.of<ExploreBloc>(context, listen: false);

    return Container(
      height: size.height,
      child: StreamBuilder<List<Result>>(
          stream: bloc.pictureList,
          builder:
              (BuildContext context, AsyncSnapshot<List<Result>> snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            }
            return TinderSwapCard(
                totalNum: snapshot.data.length,
                maxWidth: size.width,
                maxHeight: size.height * 0.75,
                minWidth: size.width * 0.75,
                minHeight: size.height * 0.6,
                cardBuilder: (context, index) => CardSwipeable(
                    imageSrc: snapshot.data[index].image,
                    recipeName: snapshot.data[index].title,
                    id: snapshot.data[index].id),
                cardController: controller,
                swipeCompleteCallback:
                    (CardSwipeOrientation orientation, index) {
                  if (orientation == CardSwipeOrientation.LEFT) {
                    // do nothing
                  } else {
                    bloc.addToFavorites(
                        snapshot.data[index].id,
                        snapshot.data[index].title,
                        snapshot.data[index].image,
                        "jpg");
                  }
                  if (index == snapshot.data.length - 1) {
                    bloc.isVegan == true
                        ? bloc.getRecipe(true)
                        : bloc.getRecipe(false);
                  }
                });
          }),
    );
  }
}
