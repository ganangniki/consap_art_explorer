import 'package:consap_art_explorer/app_src/model/response_recipe_model.dart';
import 'package:consap_art_explorer/app_src/model/response_recipe_post_model.dart';

import '../../../app_src/service/api_service.dart';

import 'package:rxdart/rxdart.dart';

class ExploreBloc {
  ExploreBloc() {
    isVeganListener(false);
    getRecipe(false);
  }

  final BehaviorSubject<List<Result>> _recipeList =
      BehaviorSubject<List<Result>>();

  final BehaviorSubject<bool> _isVegan = BehaviorSubject<bool>();

  Stream<List<Result>> get pictureList => _recipeList.stream;

  Stream<bool> get isVegan => _isVegan.stream;

  Function(bool) get isVeganListener => _isVegan.sink.add;

  Future<void> getRecipe(bool isVegan) async {
    _recipeList.sink.add(null);

    final List<Result> response = await ApiService.fetchRecipes(isVegan);

    _recipeList.sink.add(response);
  }

  void addToFavorites(
      int id, String title, String image, String imageType) async {
    final ResponseRecipePost response =
        await ApiService.postRecipe(id, title, image, imageType);
  }

  void dispose() {
    _recipeList.close();
    _isVegan.close();
  }
}
