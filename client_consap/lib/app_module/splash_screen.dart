import 'dart:async';

import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    startTimer();
  }

  startTimer() async {
    return Timer(Duration(seconds: 4), transition);
  }

  transition() {
    Navigator.pushReplacementNamed(context, '/explore');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Image.asset('assets/logo.png', height: 250),
            SizedBox(height: 200),
            CircularProgressIndicator(
                backgroundColor: Colors.teal[200], strokeWidth: 5)
          ]),
        ));
  }
}
