import 'package:consap_art_explorer/app_module/favorites/bloc/favorites_bloc.dart';
import 'package:consap_art_explorer/app_module/favorites/components/like_card.dart';
import 'package:consap_art_explorer/app_src/model/response_favorite_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FavoritesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    final FavoritesBloc bloc =
        Provider.of<FavoritesBloc>(context, listen: false);

    return ListView(padding: EdgeInsets.only(bottom: 10), children: [
      Padding(
        padding: const EdgeInsets.only(top: 20),
        child: StreamBuilder<List<ResponseFavorites>>(
            stream: bloc.favorites,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text("0 Likes",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold)),
                      Text("Top Recipes",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold))
                    ]);
              }
              var totalData = snapshot.data.length;
              return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text("$totalData Likes",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                    Text("Top Recipes",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold))
                  ]);
            }),
      ),
      SizedBox(height: 10),
      Divider(thickness: 0.8),
      Padding(
        padding: const EdgeInsets.only(left: 5, right: 5),
        child: StreamBuilder<List<ResponseFavorites>>(
            stream: bloc.favorites,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Padding(
                  padding: const EdgeInsets.only(top: 200.0),
                  child: Center(child: CircularProgressIndicator()),
                );
              } else if (snapshot.data.length < 1) {
                return Column(children: [
                  Image.asset('assets/no_recipe.gif'),
                  Text('you dont have any favorite recipe yet')
                ]);
              }
              return Wrap(
                spacing: 5,
                runSpacing: 5,
                children: List.generate(snapshot.data.length, (index) {
                  return LikeCard(
                      imageSrc: snapshot.data[index].image,
                      title: snapshot.data[index].title,
                      id: snapshot.data[index].id);
                }),
              );
            }),
      )
    ]);
  }
}
