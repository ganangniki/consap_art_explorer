import 'package:consap_art_explorer/app_module/favorites/bloc/favorites_bloc.dart';
import 'package:consap_art_explorer/app_module/favorites/screen/favorites_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class FavoritesApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<FavoritesBloc>(
        create: (_) => FavoritesBloc(),
        dispose: (_, FavoritesBloc bloc) => bloc.dispose(),
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                      onTap: () =>
                          Navigator.pushReplacementNamed(context, '/explore'),
                      child: SvgPicture.asset('assets/explore.svg',
                          height: 35, color: Colors.grey[500])),
                  GestureDetector(
                      child:
                          SvgPicture.asset('assets/favorite.svg', height: 25)),
                  GestureDetector(
                      onTap: () =>
                          Navigator.pushReplacementNamed(context, '/account'),
                      child: SvgPicture.asset('assets/profile.svg',
                          height: 25, color: Colors.grey[500]))
                ]),
          ),
          body: FavoritesScreen(),
        ));
  }
}
