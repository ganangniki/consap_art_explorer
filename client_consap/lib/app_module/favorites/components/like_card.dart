import 'package:flutter/material.dart';

class LikeCard extends StatelessWidget {
  const LikeCard({this.imageSrc, this.title, this.id});
  final String imageSrc, title, id;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    String detailUri = title.replaceAll(' ', '-') + '-$id';

    return GestureDetector(
        onTap: () => Navigator.pushNamed(context, '/detail/$detailUri'),
        child: Container(
          width: (size.width - 15) / 2,
          height: 250,
          child: Stack(children: [
            Container(
              width: (size.width - 15) / 2,
              height: 250,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: NetworkImage(imageSrc), fit: BoxFit.cover)),
            ),
            Container(
              width: (size.width - 15) / 2,
              height: 250,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  gradient: LinearGradient(colors: [
                    Colors.black.withOpacity(0.25),
                    Colors.black.withOpacity(0),
                  ], end: Alignment.topCenter, begin: Alignment.bottomCenter)),
              child:
                  Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8, bottom: 8),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(Icons.favorite, color: Colors.white, size: 15),
                        SizedBox(width: 5),
                        Flexible(
                            child: Text(
                          title,
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ))
                      ]),
                )
              ]),
            )
          ]),
        ));
  }
}
