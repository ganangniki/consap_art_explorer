import 'package:consap_art_explorer/app_src/model/response_favorite_model.dart';
import 'package:consap_art_explorer/app_src/service/api_service.dart';
import 'package:rxdart/rxdart.dart';

class FavoritesBloc {
  FavoritesBloc() {
    getFavorites();
  }

  final BehaviorSubject<List<ResponseFavorites>> _favorites =
      BehaviorSubject<List<ResponseFavorites>>();

  Stream<List<ResponseFavorites>> get favorites => _favorites.stream;

  Future<void> getFavorites() async {
    _favorites.sink.add(null);

    final List<ResponseFavorites> response = await ApiService.fetchFavorites();

    _favorites.sink.add(response);
  }

  void dispose() {
    _favorites.close();
  }
}
