import 'package:consap_art_explorer/app_module/splash_screen.dart';

import 'account/account_app.dart';
import 'detail/detail_app.dart';
import 'explore/explore_app.dart';
import 'favorites/favorites_app.dart';

import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart' show Handler, FluroRouter, TransitionType;

class Routes {
  Routes() {
    final FluroRouter router = FluroRouter();
    routesInit(router);

    runApp(MaterialApp(
      title: 'Consap recipe explorer',
      debugShowCheckedModeBanner: false,
      onGenerateRoute: router.generator,
      home: SplashScreen(),
    ));
  }

  void routesInit(FluroRouter router) {
    router.define('/',
        handler: splashScreenHandler, transitionType: TransitionType.fadeIn);
    router.define('/explore',
        handler: homeHandler, transitionType: TransitionType.fadeIn);
    router.define('/favorites',
        handler: favoriteHandler, transitionType: TransitionType.fadeIn);
    router.define('/detail/:detailUri',
        handler: detailHandler, transitionType: TransitionType.fadeIn);
    router.define('/account',
        handler: accountHandler, transitionType: TransitionType.fadeIn);
  }

  Handler splashScreenHandler =
      Handler(handlerFunc: (context, parameters) => SplashScreen());
  Handler homeHandler = Handler(handlerFunc: (context, params) => ExploreApp());
  Handler favoriteHandler =
      Handler(handlerFunc: (context, parameters) => FavoritesApp());
  Handler detailHandler = Handler(
      handlerFunc: (context, parameters) =>
          DetailApp(detailUri: parameters['detailUri'][0]));
  Handler accountHandler =
      Handler(handlerFunc: (context, parameters) => AccountApp());
}
