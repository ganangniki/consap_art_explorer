import 'package:consap_art_explorer/app_module/favorites/bloc/favorites_bloc.dart';
import 'package:consap_art_explorer/app_src/model/response_favorite_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AccountScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final FavoritesBloc bloc =
        Provider.of<FavoritesBloc>(context, listen: false);

    return Stack(fit: StackFit.expand, children: [
      Positioned(
          top: 0,
          child: Container(
            height: size.height * 0.7,
            width: size.width,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/profile_pic_dummy.jpeg'),
                    fit: BoxFit.cover)),
          )),
      Positioned(
          bottom: 0,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
            height: size.height * 0.30,
            width: size.width,
            child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(children: [
                    Text('John Doe',
                        style: TextStyle(
                            fontSize: 30, fontWeight: FontWeight.bold)),
                    Text('Freelance',
                        style: TextStyle(
                            fontSize: 15, fontStyle: FontStyle.italic))
                  ]),
                  StreamBuilder<List<ResponseFavorites>>(
                      stream: bloc.favorites,
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                AccountInfo(
                                    properties: 'recipes liked', value: '0'),
                                Container(
                                    height: 35, width: 1, color: Colors.grey),
                                AccountInfo(
                                    properties: 'Location', value: 'Jakarta'),
                              ]);
                        }
                        return Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              AccountInfo(
                                  properties: 'recipes liked',
                                  value: snapshot.data.length.toString()),
                              Container(
                                  height: 35, width: 1, color: Colors.grey),
                              AccountInfo(
                                  properties: 'Location', value: 'Jakarta'),
                            ]);
                      }),
                ]),
          ))
    ]);
  }
}

class AccountInfo extends StatelessWidget {
  const AccountInfo({this.properties, this.value});
  final String properties, value;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Text(
        properties,
        style: TextStyle(
            color: Colors.grey[500], fontStyle: FontStyle.italic, fontSize: 15),
      ),
      SizedBox(height: 10),
      Text(value, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20))
    ]);
  }
}
