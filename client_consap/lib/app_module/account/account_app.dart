import 'package:consap_art_explorer/app_module/account/screens/account_screen.dart';
import 'package:consap_art_explorer/app_module/favorites/bloc/favorites_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class AccountApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<FavoritesBloc>(
      create: (_) => FavoritesBloc(),
      dispose: (_, FavoritesBloc bloc) => bloc.dispose(),
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.grey[300],
            centerTitle: true,
            title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    onTap: () =>
                        Navigator.pushReplacementNamed(context, '/explore'),
                    child: SvgPicture.asset('assets/explore.svg',
                        height: 35, color: Colors.grey[500]),
                  ),
                  GestureDetector(
                    onTap: () =>
                        Navigator.pushReplacementNamed(context, '/favorites'),
                    child: SvgPicture.asset('assets/favorite.svg',
                        height: 25, color: Colors.grey[500]),
                  ),
                  GestureDetector(
                    child: SvgPicture.asset('assets/profile.svg', height: 25),
                  )
                ]),
          ),
          body: AccountScreen()),
    );
  }
}
